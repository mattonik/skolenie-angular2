import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component'
import { UserModule } from './user/user.module'
import { ProjectModule } from './project/project.module';

@NgModule({
    imports: [
        BrowserModule, UserModule, ProjectModule
    ],
    declarations: [
        AppComponent
    ],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule {};
