import { Component } from "@angular/core";
import { User } from "./user";
import { UserService } from "./user.service";

@Component({
  selector: 'user-login',
  templateUrl: './user-login.component.html'
})
export class UserLogin {
  public user: User = new User('', '');

  constructor(private userService: UserService) {
  }

  onSubmit() {
    console.log('user login credentials', this.user);
    this.userService.login(this.user).subscribe(
      userToken => console.log('uspech', userToken),
      error => console.log('chyba', error),
      () => console.log('hotovo')
    );
  }
}

