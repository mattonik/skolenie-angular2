import { Injectable } from "@angular/core";
import { User } from "./user";
import { UserToken } from "./user-token";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { BehaviorSubject } from "rxjs";

@Injectable()
export class UserService {
  userToken: UserToken = { login: '', token: ''};
  isLoggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(private http: HttpClient){
    const userToken = window.localStorage.getItem('userToken');
    if( userToken ) {
      this.userToken = JSON.parse(userToken);
      this.isLoggedIn.next(true);
    }
  }

  login(user: User): Observable<UserToken> {
    const url = 'https://tracking.skolenia-kurzy.net/api/login';
    console.log('user service login', user);

    return this.http
      .post<UserToken>(url, user)
      .do(userToken => {
        this.userToken = userToken;
        this.isLoggedIn.next(true);
        
        window.localStorage.setItem('userToken', JSON.stringify(userToken));
      });
  }
}