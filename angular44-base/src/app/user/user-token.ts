export interface UserToken {
  login: string;
  token: string;
}