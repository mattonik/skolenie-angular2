import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { UserLogin } from "./user-login.component";
import { UserService } from "./user.service";
import { AuthInterceptor } from "./auth-interceptor";

@NgModule({
  imports: [
    CommonModule, FormsModule, HttpClientModule
  ],
  declarations: [
    UserLogin
  ],
  exports: [
    UserLogin
  ],
  providers: [
    UserService,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true}
  ]
})
export class UserModule {

}

