import { Component } from '@angular/core';

@Component({
    selector: 'app-root',
    template: `<h1>{{title}}</h1>
    <user-login></user-login>
    <project-list></project-list>`
})
export class AppComponent {
    title: string = 'Hello Webpack';
}