import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { UserService } from "./user.service";
import { Injectable } from "@angular/core";

@Injectable()
export class LoggedInGuard implements CanActivate {
    constructor(private svc: UserService, private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (!this.svc.userToken || !this.svc.userToken.token) {
            this.router.navigate(['/login', { returnTo: state.url }]);
        }
        return !!(this.svc.userToken && this.svc.userToken.token);
    }
}
