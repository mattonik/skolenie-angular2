import { HttpInterceptor } from "@angular/common/http";
import { Injector, Injectable } from "@angular/core";
import { HttpRequest } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { HttpHandler } from "@angular/common/http";
import { HttpEvent } from "@angular/common/http";
import { UserService } from "./user.service";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    constructor(private injector: Injector) { }

    intercept(req: HttpRequest<any>, next: HttpHandler) : Observable<HttpEvent<any>> {
        const userService = this.injector.get(UserService);
        if (userService.userToken && userService.userToken.token) {
            const clonedReq = req.clone({
                headers: req.headers.set('x-tasks-user-token', userService.userToken.token)
            });
            return next.handle(clonedReq);
        }
        return next.handle(req);
    }
}