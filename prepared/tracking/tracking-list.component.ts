import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { TrackingService } from "./tracking.service";
import { ProjectService } from "../project/project.service";
import { Tracking } from "./tracking";
import { Project } from "../project/project";

@Component({
    selector: 'tracking-list',
    templateUrl: './tracking-list.component.html'
})
export class TrackingList implements OnInit {
    trackings: Tracking[];
    tracking: Tracking;
    projects: Project[];

    constructor(
        private trackingService: TrackingService,
        private projectService: ProjectService
    ) { }

    ngOnInit() {
        this.trackingService.getTrackings()
            .subscribe(trackings => {
                this.trackings = trackings;
                this.tracking = trackings.find(t => !t.end);
            });

        this.projectService.getProjects()
            .subscribe(projects => this.projects = projects);
    }
}