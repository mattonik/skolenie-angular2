import { Directive, HostBinding, HostListener, ElementRef, Input } from '@angular/core';

@Directive({
    selector: 'highlightColor'
})
export class Highlighter {
    @Input('highlightColor')
    color: string = 'lightblue';

    highlight(c: string) {
        let element = this.element.nativeElement;
        element.style.backgroundColor = c;
    }

    @HostBinding('class.editing') editing: boolean;

    @HostListener('focus', ['$event']) onFocus(e: any) {
        console.log('focus', e);
        this.editing = true;
        this.highlight(this.color);
    }

    @HostListener('blur') onBlur() {
        console.log('blur');
        this.editing = false;
        this.highlight(null);
    }

    constructor(private element: ElementRef) {
    }
}