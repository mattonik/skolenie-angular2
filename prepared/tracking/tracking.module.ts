import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { TrackingService } from "./tracking.service";
import { TrackingList } from "./tracking-list.component";

@NgModule({
    imports: [ CommonModule, HttpClientModule, FormsModule ],
    providers: [ TrackingService ],
    declarations: [ TrackingList ],
    exports: [ TrackingList ]
})
export class TrackingModule {
}