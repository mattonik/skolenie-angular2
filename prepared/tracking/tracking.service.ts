import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Tracking } from "./tracking";
import { Observable } from "rxjs/Observable";

@Injectable()
export class TrackingService {
    constructor(private http: HttpClient) {}

    getTrackings(): Observable<Tracking[]> {
        return this.http
            .get<Tracking[]>('https://tracking.skolenia-kurzy.net/api/tracking')
    }

    start(project: string, title: string): Observable<Tracking[]> {
        const url = 'https://tracking.skolenia-kurzy.net/api/tracking';
        const data = { project, title };
        
        return this.http.post<Tracking[]>(url, data);
    }

    stop(id: string): Observable<Tracking[]> {
        const url = `https://tracking.skolenia-kurzy.net/api/tracking/${id}`;
        const data = {};
        
        return this.http.post<Tracking[]>(url, data);
    }

}