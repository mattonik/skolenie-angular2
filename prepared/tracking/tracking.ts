export interface Tracking {
    id: string,
    project: string,
    title: string,
    start: Date,
    end?: Date
}