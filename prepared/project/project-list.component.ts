import { Component, OnInit } from "@angular/core";
import { ProjectService } from "./project.service";
import { Project } from "./project";

@Component({
    selector: 'project-list',
    templateUrl: './project-list.component.html'
})
export class ProjectList {
    projects: Project[];

    constructor(private projectService: ProjectService) {
    }
}