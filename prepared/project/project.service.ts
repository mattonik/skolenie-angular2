import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { Project } from './project';

@Injectable()
export class ProjectService {
    constructor(private http: HttpClient) { }
}