import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { ProjectService } from "./project.service";
import { ProjectList } from "./project-list.component";

@NgModule({
    imports: [ CommonModule, HttpClientModule, FormsModule ],
    providers: [ ProjectService ],
    declarations: [ ProjectList ],
    exports: [ ProjectList ]
})
export class ProjectModule {
}